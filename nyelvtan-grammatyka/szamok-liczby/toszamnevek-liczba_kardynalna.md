# __Liczby kardynalne__ (_tőszámnevek: egy, kettő.._)

## Bevezetés

A lengyelben a számok hasonlóképp viselkednek, mint a melléknevek. Ez azt jelenti például, hogy a főnévvel együtt ragozódnak. Például:

- __Zjem dwa jabłka codziennie.__ - _Két almát eszem meg naponta. (alanyeset)_
- __Nie zjem dwóch(!) jabłek codziennie.__ - _Nem eszem meg két almát naponta. (birtokos eset)_

Azonban itt beleszólnak már a mennyiségek illetve kicsit más ragokat is kapnak. Jelenlegi tudásom szerint (2018. Febr. 6.) ezt nem tudom itt megfelelően kifejteni.

## Számalakok

### Számok 1-től 10-ig

Az alábbi táblázatban azokat a tőszámneveket látjuk felsorolva, amik a semleges és nőnemhez tartoznak. Amint egy "személyként" meghatározott dologról beszélünk (pl. lekarz, policjant, .. bałwan), plusz __KÉT__ nőnemű dologról __megváltozik ez az alak__.

|liczba (numer)|tekst       |
|-------------:|:-----------|
|             1|   __jeden__|
|             2|     __dwa__|
|             3|    __trzy__|
|             4|  __cztery__|
|             5|    __pięć__|
|             6|   __sześć__|
|             7|  __siedem__|
|             8|   __osiem__|
|             9|__dziewięć__|
|            10|__dziesięć__|

### Számok 11-től 19-ig

Többségében a szótövet levágjuk és [__-naście__] ragot kapnak. A rag az fix, de ami előtte van arra nincs szabály. Néhány helyen a párjára vált a hang (hangtan!) vagy egyszerűen levágódik. Megpróbáltam magyarázni amennyire lehet, de az csak tájékoztató jellegű, érdekesség!

|liczba (numer)|tekst             |    objaśnienie|
|-------------:|:-----------------|--------------:|
|            11|    __jedenaście__|       -n levág|
|            12|     __dwanaście__|              -|
|            13|    __trzynaście__|              -|
|            14|   __czternaście__|       -y levág|
|            15|    __piętnaście__|          ć → t|
|            16|    __szesnaście__| ć levág, ś → s|
|            17|  __siedemnaście__|              -|
|            18|   __osiemnaście__|              -|
|            19|__dziewiętnaście__|          ć → t|

### Tizes számok (10, 20, stb..)

Itt nem érdemes szabályról beszéni, jobban járunk, ha bebiflázzuk.

|liczba (numer)|tekst               |
|-------------:|:-------------------|
|            10|        __dziesięć__|
|            20|     __dwadzieścia__|
|            30|     __trzydzieści__|
|            40|    __czterdzieści__|
|            50|    __pięćdziesiąt__|
|            60|   __sześćdziesiąt__|
|            70|  __siedemdziesiąt__|
|            80|   __osiemdziesiąt__|
|            90|__dziewięćdziesiąt__|
|           100|             __sto__|

### Százas számok (100, 200, stb..)

Itt nem érdemes szabályról beszéni, jobban járunk, ha bebiflázzuk.

|liczba (numer)|tekst          |
|-------------:|:--------------|
|           100|        __sto__|
|           200|   __dwieście__|
|           300|    __trzysta__|
|           400|  __czterysta__|
|           500|    __pięćset__|
|           600|   __sześćset__|
|           700|  __siedemset__|
|           800|   __osiemset__|
|           900|__dziewięćset__|
|          1000|     __tysiąc__|

## Összetett számok képzése

Egyszerűen ugyanúgy működik, mint a magyarban.

- __1526__ - _tysiąc pięćset dwadzieścia sześć (ezerötszázhuszonhat)_
- __333__ - _trzysta trzydzieści trzy (háromszázharminchárom)_
